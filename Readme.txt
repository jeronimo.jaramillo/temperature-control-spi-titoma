Project: Temperature control with SPI and UART. 
Author: Jeronimo Jaramillo B
Date: 15/05/22

This project will measure the temperature with the BMP280 sensor and has a parse command, whose commands are:

'E'---> stops the sensor temperature measurement

'F'---> starts the sensor temperature measurement

In the same way, it has a control action for temperature. If the temperature is greater than 40°C, the LDE4 of the board is activated