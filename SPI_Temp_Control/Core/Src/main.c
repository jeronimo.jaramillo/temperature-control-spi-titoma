/* USER CODE BEGIN Header */
/**
******************************************************************************
* @file           : main.c
* @brief          : Main program body
******************************************************************************
* @attention
*
* Copyright (c) 2022 STMicroelectronics.
* All rights reserved.
*
* This software is licensed under terms that can be found in the LICENSE file
* in the root directory of this software component.
* If no LICENSE file comes with this software, it is provided AS-IS.
*
******************************************************************************
*/
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */


////////////////// Structure for the Sensor register address/////////////////////////////////////////7
typedef enum bmp280_register_address_ {

BMP280_REG_ADDR_ERR = 0x00,
BMP280_REG_ADDR_CAL_START_ADDR = 0x88,
BMP280_REG_ADDR_ID = 0xD0,
BMP280_REG_ADDR_RESET = 0xE0,
BMP280_REG_ADDR_STATUS = 0xF3,
BMP280_REG_ADDR_CTRL_MEAS = 0xF4,
BMP280_REG_ADDR_CONFIG = 0xF5,
BMP280_REG_ADDR_PRESS_MSB = 0xF7,
BMP280_REG_ADDR_PRESS_LBS = 0xF8,
BMP280_REG_ADDR_PRESS_XLBS = 0xF9,
BMP280_REG_ADDR_TEMP_MSB = 0xFA,
BMP280_REG_ADDR_TEMP_LBS = 0xFB,
BMP280_REG_ADDR_TEMP_XLBS = 0xFC,

} bmp280_register_address_t;



//////////////////////// Data calibration registers address//////////////////////////////
typedef struct bmp_calibration_data_t {

uint16_t dig_T1;
int16_t dig_T2;
int16_t dig_T3;
uint16_t dig_P1;
int16_t dig_P2;
int16_t dig_P3;
int16_t dig_P4;
int16_t dig_P5;
int16_t dig_P6;
int16_t dig_P7;
int16_t dig_P8;
int16_t dig_P9;

} bmp_calibration_data_t;



SPI_HandleTypeDef hspi1;
UART_HandleTypeDef huart1;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);


int main(void) {

/* MCU Configuration--------------------------------------------------------*/
/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
HAL_Init();

/* Configure the system clock */
SystemClock_Config();

/* Initialize all configured peripherals */
MX_GPIO_Init();
MX_SPI1_Init();
MX_USART1_UART_Init();

/* USER CODE BEGIN 2 */


/////////////////////////////////////// Variables //////////////////////////////////////////////
uint8_t spi_tx_data_reg = 0x00, i = 0;
uint8_t spi_rx_data[6] = { 0x00 };
uint8_t bmp280_Calib_data[5] = { 0x00 };
int8_t ctrl_meas_bits = 0x00;
int32_t raw_temperature = 0;

float temp_degr = 0;
double var1 = 0;
double var2 = 0;

char temp_char_num [5];
char skip[] = "\r\n";
char MGS_star[] = {"Temperature Control system \r\n"};
char MGS_parse_CMD[] = {"Put: F to start the temperature measurement or E to stop the temperature measurement\r\n"};
char MGS_Meas[]= {"The temperature is: \r\n"};
char MGS_stop[]= {"The temperature measurement is stop\r\n"};
char MGS_Alert[]= {"High Temperature: \r\n"};

uint16_t dig_T1 = 0b0110100110100011;
int16_t dig_T2 = 0b0110101001000000;
int16_t dig_T3 = 0b1111110000011000;

uint8_t rx_data = 0;
uint8_t toogle_meas_enable = 0;
uint32_t toggle_period_ms = 0;
uint32_t meas_tick = 0;

///////////////////////////////////////// Configuration of the BMP280 temperature sensor ///////////////////////////////
// Search the sensor ID
spi_tx_data_reg = BMP280_REG_ADDR_ID;
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 100) == HAL_OK) {
	if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 100) == HAL_OK) {
		HAL_Delay(100);
	}
}
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);

// Change the Config sensor (temp over samp x2, skipped the press meas, normal power mode)
uint8_t ctrl_meas_Bytes = 0x23;
uint8_t spi_config[2] = { 0x74, ctrl_meas_Bytes };

HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
if (HAL_SPI_Transmit(&hspi1, spi_config, 2, 100) == HAL_OK) {
	HAL_Delay(100);
}
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);

// Verify the CTRL_MEAS register configuration
spi_tx_data_reg = BMP280_REG_ADDR_CTRL_MEAS;
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 100) == HAL_OK) {
	if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 100) == HAL_OK) {
		HAL_Delay(100);
	}

}
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);
HAL_Delay(100);

// Extraction of dig_T1, dig_T2 and dig_T3
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
for (i=0; i < 6; i++) {
	spi_tx_data_reg = BMP280_REG_ADDR_CAL_START_ADDR + i;
	if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 100) == HAL_OK) {
		if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 100) == HAL_OK) {
			HAL_Delay(100);
			bmp280_Calib_data[i] = spi_rx_data[0];
		}
	}
	HAL_Delay(100);
}
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);

/////////////////////////////////Send Startup message ///////////////////////////////////////////
HAL_UART_Transmit(&huart1,MGS_star,sizeof(MGS_star)-1, 500);

/////////////////////////////////Send instruction message fot the parse command/////////////////
HAL_UART_Transmit(&huart1,MGS_parse_CMD,sizeof(MGS_parse_CMD)-1, 500);

/* USER CODE END 2 */

/* Infinite loop */
/* USER CODE BEGIN WHILE */
while (1) {

	/* USER CODE END WHILE */
	if (HAL_UART_Receive(&huart1, &rx_data, 1, 500) == HAL_OK) {   // Wait for the letter command


		// Parse the command
		switch (rx_data) {

		case 'E': // Stop to read
			HAL_UART_Transmit(&huart1,MGS_stop,sizeof(MGS_stop)-1, 500);	// Send Stop message
			toogle_meas_enable = 0;											// disable the temperature measurement
			HAL_GPIO_WritePin(LED_4_GPIO_Port, LED_4_Pin, GPIO_PIN_RESET);	// Put the LED OFF
			HAL_Delay(100);
			break;


		case 'F':  // Read temperature
			HAL_UART_Transmit(&huart1,MGS_Meas,sizeof(MGS_Meas)-1, 500);	// Send the temperature message
			toogle_meas_enable = 1;											// Enable the temperature measurement
			toggle_period_ms = 1000;										// Set the measurement period
			break;

		default:
			// the command was ignored
			break;
		}
	}

	if (toogle_meas_enable) {
		if ((HAL_GetTick() - meas_tick) > toggle_period_ms) {    //temperature measurement
			meas_tick = HAL_GetTick();							 // Evaluate the period

///////////////// Take the temperature measurement /////////////////////////////////////////
			spi_tx_data_reg = BMP280_REG_ADDR_TEMP_MSB;
			HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin,GPIO_PIN_RESET);
			if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 100)
					== HAL_OK) {
				if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 100)
						== HAL_OK) {
					HAL_Delay(100);
					raw_temperature =
							(int32_t) (spi_rx_data[2] & 0b00001111)
									| (spi_rx_data[1] << 4)
									| (spi_rx_data[0] << 12);


/////////////////////////////////////// Implementation of the transformation equation /////////////////////////////////
					var1 = (((double) raw_temperature) / 16384.0
							- ((double) dig_T1) / 1024.0)
							* ((double) dig_T2);

					var2 = ((double) raw_temperature) / 131072.0
							- ((double) dig_T1) / 8192.0;

					var2 = (var2 * var2) * ((double) dig_T3);

					temp_degr = (float) (var1 + var2) / 5120.0;

/////////////////////////////////////////////// Send the value of the degrees on the UART /////////////////////////////////
					sprintf(temp_char_num, "%f", temp_degr);
					HAL_UART_Transmit(&huart1,temp_char_num,sizeof(temp_char_num), 500);
					HAL_UART_Transmit(&huart1,skip,sizeof(skip)-1, 500);

////////////////////////////////////////////// Control Action //////////////////////////////////////////////////
					if (temp_degr > 40.0){											   // For high temperature
						HAL_GPIO_WritePin(LED_4_GPIO_Port, LED_4_Pin, GPIO_PIN_SET);
						HAL_UART_Transmit(&huart1,MGS_Alert,sizeof(MGS_Alert)-1, 500);

					}

					if (temp_degr < 40.0){											   // For low temperature
						HAL_GPIO_WritePin(LED_4_GPIO_Port, LED_4_Pin, GPIO_PIN_RESET);
					}

				}
			}
			HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin,GPIO_PIN_SET);
			HAL_Delay(100);

		}

	}
	/* USER CODE BEGIN 3 */
}
/* USER CODE END 3 */
}

/**
* @brief System Clock Configuration
* @retval None
*/
void SystemClock_Config(void) {
RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

/** Configure the main internal regulator output voltage
 */
__HAL_RCC_PWR_CLK_ENABLE();
__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

/** Initializes the RCC Oscillators according to the specified parameters
 * in the RCC_OscInitTypeDef structure.
 */
RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
RCC_OscInitStruct.HSIState = RCC_HSI_ON;
RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
RCC_OscInitStruct.PLL.PLLM = 8;
RCC_OscInitStruct.PLL.PLLN = 64;
RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
RCC_OscInitStruct.PLL.PLLQ = 7;
if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	Error_Handler();
}

/** Initializes the CPU, AHB and APB buses clocks
 */
RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
		| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
	Error_Handler();
}
}

/**
* @brief SPI1 Initialization Function
* @param None
* @retval None
*/
static void MX_SPI1_Init(void) {

/* USER CODE BEGIN SPI1_Init 0 */

/* USER CODE END SPI1_Init 0 */

/* USER CODE BEGIN SPI1_Init 1 */

/* USER CODE END SPI1_Init 1 */
/* SPI1 parameter configuration*/
hspi1.Instance = SPI1;
hspi1.Init.Mode = SPI_MODE_MASTER;
hspi1.Init.Direction = SPI_DIRECTION_2LINES;
hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
hspi1.Init.NSS = SPI_NSS_SOFT;
hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
hspi1.Init.CRCPolynomial = 10;
if (HAL_SPI_Init(&hspi1) != HAL_OK) {
	Error_Handler();
}
/* USER CODE BEGIN SPI1_Init 2 */

/* USER CODE END SPI1_Init 2 */

}

/**
* @brief USART1 Initialization Function
* @param None
* @retval None
*/
static void MX_USART1_UART_Init(void) {

/* USER CODE BEGIN USART1_Init 0 */

/* USER CODE END USART1_Init 0 */

/* USER CODE BEGIN USART1_Init 1 */

/* USER CODE END USART1_Init 1 */
huart1.Instance = USART1;
huart1.Init.BaudRate = 115200;
huart1.Init.WordLength = UART_WORDLENGTH_8B;
huart1.Init.StopBits = UART_STOPBITS_1;
huart1.Init.Parity = UART_PARITY_NONE;
huart1.Init.Mode = UART_MODE_TX_RX;
huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
huart1.Init.OverSampling = UART_OVERSAMPLING_16;
if (HAL_UART_Init(&huart1) != HAL_OK) {
	Error_Handler();
}
/* USER CODE BEGIN USART1_Init 2 */

/* USER CODE END USART1_Init 2 */

}

/**
* @brief GPIO Initialization Function
* @param None
* @retval None
*/
static void MX_GPIO_Init(void) {
GPIO_InitTypeDef GPIO_InitStruct = { 0 };

/* GPIO Ports Clock Enable */
__HAL_RCC_GPIOH_CLK_ENABLE();
__HAL_RCC_GPIOA_CLK_ENABLE();
__HAL_RCC_GPIOC_CLK_ENABLE();
__HAL_RCC_GPIOG_CLK_ENABLE();

/*Configure GPIO pin Output Level */
HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);

/*Configure GPIO pin Output Level */
HAL_GPIO_WritePin(LED_4_GPIO_Port, LED_4_Pin, GPIO_PIN_RESET);

/*Configure GPIO pin : bmp280_CS_Pin */
GPIO_InitStruct.Pin = bmp280_CS_Pin;
GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
HAL_GPIO_Init(bmp280_CS_GPIO_Port, &GPIO_InitStruct);

/*Configure GPIO pin : LED_4_Pin */
GPIO_InitStruct.Pin = LED_4_Pin;
GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
HAL_GPIO_Init(LED_4_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
* @brief  This function is executed in case of error occurrence.
* @retval None
*/
void Error_Handler(void) {
/* USER CODE BEGIN Error_Handler_Debug */
/* User can add his own implementation to report the HAL error return state */
__disable_irq();
while (1) {
}
/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t *file, uint32_t line)
{
/* USER CODE BEGIN 6 */
/* User can add his own implementation to report the file name and line number,
 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

